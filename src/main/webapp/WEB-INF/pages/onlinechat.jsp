<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" pageEncoding="UTF-8" isELIgnored="false" %>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="format-detection" content="telephone=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta content="yes" name="apple-mobile-web-app-capable">
    <meta content="yes" name="apple-touch-fullscreen">
    <meta name="full-screen" content="yes">
    <meta content="default" name="apple-mobile-web-app-status-bar-style">
    <meta name="screen-orientation" content="portrait">
    <meta name="browsermode" content="application">
    <meta name="msapplication-tap-highlight" content="no">
    <meta name="x5-orientation" content="portrait">
    <meta name="x5-fullscreen" content="true">
    <meta name="x5-page-mode" content="app">
    <base target="_blank">

    <title>onlinechat-demo</title>
    <link rel="shortcut icon" href="<c:url value="/static/iocn/onlinechat-favicon.ico"/>" type="image/x-icon"/>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://ajax.aspnetcdn.com/ajax/jquery/jquery-3.5.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sockjs-client@1/dist/sockjs.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
            integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
            crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/js/bootstrap.min.js"
            integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
            crossorigin="anonymous"></script>

    <%--css for this jsp!--%>
    <link rel="stylesheet" href="<c:url value="/static/css/onlinechat.css"/>">

    <style>
        body {
            position: relative;
        }

        #btn-chat {
            position: fixed;
            top: 48px;
            right: 100px;
        }

    </style>

</head>

<body>
<button id="btn-chat" type="button" class="btn btn-danger" data-toggle="modal" data-target="#staticBackdrop">
    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-chat-square-quote-fill" viewBox="0 0 16 16">
        <path d="M0 2a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v8a2 2 0 0 1-2 2h-2.5a1 1 0 0 0-.8.4l-1.9 2.533a1 1 0 0 1-1.6 0L5.3 12.4a1 1 0 0 0-.8-.4H2a2 2 0 0 1-2-2V2zm7.194 2.766a1.688 1.688 0 0 0-.227-.272 1.467 1.467 0 0 0-.469-.324l-.008-.004A1.785 1.785 0 0 0 5.734 4C4.776 4 4 4.746 4 5.667c0 .92.776 1.666 1.734 1.666.343 0 .662-.095.931-.26-.137.389-.39.804-.81 1.22a.405.405 0 0 0 .011.59c.173.16.447.155.614-.01 1.334-1.329 1.37-2.758.941-3.706a2.461 2.461 0 0 0-.227-.4zM11 7.073c-.136.389-.39.804-.81 1.22a.405.405 0 0 0 .012.59c.172.16.446.155.613-.01 1.334-1.329 1.37-2.758.942-3.706a2.466 2.466 0 0 0-.228-.4 1.686 1.686 0 0 0-.227-.273 1.466 1.466 0 0 0-.469-.324l-.008-.004A1.785 1.785 0 0 0 10.07 4c-.957 0-1.734.746-1.734 1.667 0 .92.777 1.666 1.734 1.666.343 0 .662-.095.931-.26z"></path>
    </svg>
</button>

<!-- Modal -->
<div class="modal fade" id="staticBackdrop" data-backdrop="static" data-keyboard="false" tabindex="-1"
     aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content" style="background-color: rgb(245,245,245)">
            <div class="modal-header" style="height: 60px;">
                <div class="modal-title" id="staticBackdropLabel">
                    <span id="online-token" style="color: darkgreen;font-size: 35px">
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                             class="bi bi-person-bounding-box" viewBox="0 0 16 16">
                          <path d="M1.5 1a.5.5 0 0 0-.5.5v3a.5.5 0 0 1-1 0v-3A1.5 1.5 0 0 1 1.5 0h3a.5.5 0 0 1 0 1h-3zM11 .5a.5.5 0 0 1 .5-.5h3A1.5 1.5 0 0 1 16 1.5v3a.5.5 0 0 1-1 0v-3a.5.5 0 0 0-.5-.5h-3a.5.5 0 0 1-.5-.5zM.5 11a.5.5 0 0 1 .5.5v3a.5.5 0 0 0 .5.5h3a.5.5 0 0 1 0 1h-3A1.5 1.5 0 0 1 0 14.5v-3a.5.5 0 0 1 .5-.5zm15 0a.5.5 0 0 1 .5.5v3a1.5 1.5 0 0 1-1.5 1.5h-3a.5.5 0 0 1 0-1h3a.5.5 0 0 0 .5-.5v-3a.5.5 0 0 1 .5-.5z"></path>
                          <path d="M3 14s-1 0-1-1 1-4 6-4 6 3 6 4-1 1-1 1H3zm8-9a3 3 0 1 1-6 0 3 3 0 0 1 6 0z"></path>
                        </svg>
                    </span>
                    <span id="online-token-username">${sessionScope.username}</span></div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>


            <div class="modal-body">
                <div class="container-fluid">
                    <div class="row">
                        <!--消息框-->
                        <div id="msg-box" class="col-7 col-sm-7 col-md-7 overflow-auto"></div>

                        <div class="col-5 col-sm-5 col-md-5" style="position:relative;">
                            <div id="curuser-list-info-nav">
                                <p></p>
                            </div>

                            <div class="row" id="curuser-list">
                                <div class="col-6 col-sm-6 col-md-6 overflow-auto">
                                    <div id="curuser-list-1" class="col-12 col-sm-12 col-md-12">
                                        <ul id="onlineuser-1-list" class="onlineuser-class">
                                            <%--<li></li>--%>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-6 col-sm-6 col-md-6">
                                    <div id="curuser-list-2" class="col-12 col-sm-12 col-md-12 overflow-auto">
                                        <ul id="onlineuser-2-list" class="onlineuser-class">
                                            <%--<li></li>--%>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                            <!--广播消息框-->
                            <div class="row">
                                <div id="boardcast-list" class="col-12 col-sm-12 col-md-12 overflow-auto">
                                    <div id="boardcast-list-nav">
                                        <p>广播消息</p>
                                    </div>
                                    <div id="boardcast-list-info"></div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row" style="position: relative">
                        <!--消息输入框-->
                        <div class="col-7 col-sm-7 col-md-7" id="input-msg-box">
                            <textarea id="input-msg" <%--onkeydown="sendWSMessage()"--%>></textarea>
                            <button id="sen-btn" onclick="sendWSMsg()">SEND</button>
                        </div>
                        <!--当前系统时间和在线时长-->
                        <div class="col-5 col-sm-5 col-md-5" style="width: auto;">
                            <span id="time-now"></span>
                        </div>
                    </div>
                </div>


            </div>

            <div class="modal-footer" style="margin-top: -10px">
                <small>contact: <a href="javascript:void(0);"
                                   style="text-decoration: none;">zbzou@hust.edy.cn</a></small>
            </div>
        </div>
    </div>
</div>


</body>

<script>
    var ws;
    var userSelf;
    var selectedCheckUser = "广播";

    $(function () {
        /*ui style start*/
        var innerToken = true;
        setInterval(function () {
            if (innerToken) {
                $("#online-token").css("color", "green");
                innerToken = !innerToken;
            } else {
                $("#online-token").css("color", "red")
                innerToken = !innerToken;
            }
        }, 1000);
        /*ui style end*/

        //for test!
        $("#btn-chat").click();

        $(document).on("click", ".onlineuser-class li", function (event) {
            selectedCheckUser = $(event.target).text();
            console.log(selectedCheckUser);
            $(event.target).addClass("onlineuser-cur-active").siblings().removeClass("onlineuser-cur-active");
        })

        userSelf = '<%=session.getAttribute("username")+""%>';
        webSocket(userSelf);
    })

    //websocket!!!
    function webSocket(userSelf) {
        /*Consider browser compatibility*/
        /*Maybe you can use *sockJS* instead for this!*/
        if ('WebSocket' in window) {
            ws = new WebSocket("ws://localhost:80/onlinechat/websocket");
        } else if ('MozWebSocket' in window) {
            ws = new MozWebSocket("ws://localhost:8080/onlinechat/websocket");
        } else {
            console.warn("当前浏览器不支持websocket original API!");
            ws = new SockJS("ws://localhost:8080/onlinechat/sockjs/websocket");
        }

        ws.onopen = function (evt) {
            //此处处理webscoket连接时的信息，因此可以拿到所有和连接有关的信息
            //不再作相应的处理
            console.log(evt.target);
        };

        ws.onmessage = function (evt) {
            var _data_json = JSON.parse(evt.data);
            //通过判断data的类型来觉得该信息是用户登录的提示消息还是用户发送的消息
            if (_data_json.msgType === "message") {
                //{msgType:"message",fromUser:"Tom",toUser:"Smith",content:"h'a yee rectly?!"}
                //{msgType:"message",fromUser:"xiaoming",toUser:"",content:"this is a boardcast from xiaoming"}
                showMsgForPot2Pot(_data_json, userSelf);

            } else if (_data_json.msgType === "boardcast") {
                //{msgType:"boardcast",fromUser:"",toUser:"",content:"xiaoming-xiaoli-shmith"}
                showMsgForBoardcast(_data_json, userSelf);
            }
        };

        ws.onclose = function (evt) {};
        ws.onerror = function (evt) {};
        ws.onclose = function (evt) {}
    }

    function showMsgForPot2Pot(data_json, curUser) {
        var str = "";
        //此时要判断该用户发送的消息是广播消息还是点对点消息,通过toName判断
        if (!data_json.toUser) {
            //说明是用户发送的广播消息
            $("#boardcast-list-info").append('<p style="margin-top: 0;margin-bottom: -3px">' +
                '<span style="display: inline-block;width: 100px">' + getCurrentTime() + '</span>' +
                data_json.content + "(from: " + data_json.fromUser + ')</p>');
            scroll2Bottom("#boardcast-list-info");
        } else {
            //说明是用户发送的点对点消息
            var lastChatTimeLab = $("#msg-box>p:last").text();
            //如果为null说明用户刚第一次聊天，加上聊天时间
            if (!lastChatTimeLab || checkForChatTime(lastChatTimeLab)) {
                var now = new Date();
                str += "<p class=\"label text-center\">" + now.getHours() + ':' + now.getMinutes() + "</p>"
            }

            if (data_json.fromUser === curUser) {
                //说明是自己发的消息，应该显示在消息框的右侧
                str += '<div style="position:relative;">\n' +
                    '        <button class="btn btn-success square">\n' +
                    '            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"\n' +
                    '                 class="bi bi-person-bounding-box" viewBox="0 0 16 16">\n' +
                    '                <path d="M1.5 1a.5.5 0 0 0-.5.5v3a.5.5 0 0 1-1 0v-3A1.5 1.5 0 0 1 1.5 0h3a.5.5 0 0 1 0 1h-3zM11 .5a.5.5 0 0 1 .5-.5h3A1.5 1.5 0 0 1 16 1.5v3a.5.5 0 0 1-1 0v-3a.5.5 0 0 0-.5-.5h-3a.5.5 0 0 1-.5-.5zM.5 11a.5.5 0 0 1 .5.5v3a.5.5 0 0 0 .5.5h3a.5.5 0 0 1 0 1h-3A1.5 1.5 0 0 1 0 14.5v-3a.5.5 0 0 1 .5-.5zm15 0a.5.5 0 0 1 .5.5v3a1.5 1.5 0 0 1-1.5 1.5h-3a.5.5 0 0 1 0-1h3a.5.5 0 0 0 .5-.5v-3a.5.5 0 0 1 .5-.5z"/>\n' +
                    '                <path d="M3 14s-1 0-1-1 1-4 6-4 6 3 6 4-1 1-1 1H3zm8-9a3 3 0 1 1-6 0 3 3 0 0 1 6 0z"/>\n' +
                    '            </svg>\n' +
                    '        </button>\n' +
                    '        <span style="user-select: none;cursor:pointer;position: absolute;left: 50px;top: -10px;color: rgb(178,178,178)">' + curUser + '</span>\n' +
                    '        <p class="xiaoxi-p-right wrap" style="margin-left: 50px;margin-top: -20px">\n' +
                    '            <span class="xiaoxi-right" style="">' + data_json.content + '</span></p>\n' +
                    '    </div>'
            } else if (data_json.toUser === curUser) {
                //说明是别人发给我的短信，需要展示在我的消息框的左侧
                str += '<div style="position:relative;">\n' +
                    '        <button class="btn btn-primary square">\n' +
                    '            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-person-circle" viewBox="0 0 16 16">\n' +
                    '                <path d="M11 6a3 3 0 1 1-6 0 3 3 0 0 1 6 0z"/>\n' +
                    '                <path fill-rule="evenodd" d="M0 8a8 8 0 1 1 16 0A8 8 0 0 1 0 8zm8-7a7 7 0 0 0-5.468 11.37C3.242 11.226 4.805 10 8 10s4.757 1.225 5.468 2.37A7 7 0 0 0 8 1z"/>\n' +
                    '            </svg>\n' +
                    '        </button>\n' +
                    '        <span style="user-select: none;cursor:pointer;position: absolute;left: 50px;top: -10px;color: rgb(178,178,178)">' + data_json.fromUser + '</span>\n' +
                    '        <p class="xiaoxi-p-left wrap" style="margin-left: 50px;margin-top: -20px">\n' +
                    '            <span class="xiaoxi-left" style="">' + data_json.content + '</span></p>\n' +
                    '    </div>'
            }
        }

        //将最终的消息展示在消息框msg_box
        var msgsBox = document.getElementById("msg-box");
        msgsBox.innerHTML = msgsBox.innerHTML + str;
        scroll2Bottom("#msg-box");

        //最后将当前列表框中和toName符合的用户激活显示
        $("#onlineuser-1-list li:contains(" + data_json.fromUser + ")").addClass("active").siblings().removeClass("active");

    }

    function showMsgForBoardcast(_data_json, curUser) {
        //{data:"tim,imoder",fromName:"",toName:"",content:"tom-jim-smith",flag:'true'/'flase'}

        //获取所有在线用户名称
        var userArr = _data_json.content.split("-");

        //显示所有用户数量
        $("#curuser-list-info-nav p").text("在线列表[" + userArr.length + "]");

        //显示在线用户列表
        $(".onlineuser-class").empty();
        $("#onlineuser-1-list").append('<li id="gb" class="onlineuser-list-item">广播</li>');

        //TODO: 怎么加'.active'?
        var curCount = 0;
        var flag = true;
        $.each(userArr, function (i, onlineUserName) {
            //这里如果只有当前用户一人，也是不显示的
            curCount++;
            if (onlineUserName !== curUser) {
                if (curCount < 7) {
                    //left
                    if (selectedCheckUser === onlineUserName) {
                        $("#onlineuser-1-list").append('<li class="onlineuser-list-item onlineuser-cur-active">' + onlineUserName + '</li>');
                        flag = false;
                    } else {
                        $("#onlineuser-1-list").append('<li class="onlineuser-list-item">' + onlineUserName + '</li>');
                    }
                } else {
                    if (selectedCheckUser === onlineUserName) {
                        $("#onlineuser-2-list").append('<li class="onlineuser-list-item onlineuser-cur-active">' + onlineUserName + '</li>');
                        flag = false;
                    } else {
                        $("#onlineuser-2-list").append('<li class="onlineuser-list-item">' + onlineUserName + '</li>');
                    }
                }
            }
        });

        if (flag) $("#gb").addClass("onlineuser-cur-active");

        //将除了该用户外的所有用户信息展出出来
        if (_data_json.flag) {
            //通知下线
            $("#boardcast-list-info").append('<p style="margin-top: 0;margin-bottom: -3px">' +
                '<span style="display: inline-block;width: 100px">' + getCurrentTime() + '</span>' +
                _data_json.flag + '已下线!</p>')
        } else {
            if (userArr[userArr.length - 1] !== curUser) {
                //提示某某用户上线了
                $("#boardcast-list-info").append('<p style="margin-top: 0;margin-bottom: -3px">' +
                    '<span style="display: inline-block;width: 100px">' + getCurrentTime() + '</span>' +
                    userArr[userArr.length - 1] + '已上线!</p>')
            } else {
                $("#boardcast-list-info").append('<p style="margin-top: 0;margin-bottom: -3px">' +
                    '<span style="display: inline-block;width: 100px">' + getCurrentTime() + '</span>您已上线!</p>')
            }
        }
        scroll2Bottom("#boardcast-list-info");
    }

    function checkForChatTime(lastChatTime) {
        var tmp = lastChatTime.split(":");
        return new Date().getHours() * 60 + new Date().getMinutes() - tmp[0] * 60 - tmp[1] > 5;
    }

    function getCurrentTime() {
        var curr_date = new Date();
        return (curr_date.getMonth() + 1) + "/" + curr_date.getDate() + " "
            + curr_date.getHours() + ":" + curr_date.getMinutes() + ":" + curr_date.getSeconds() + " ";
    }

    function scroll2Bottom(scrollEle) {
        $(scrollEle).scrollTop($(scrollEle)[0].scrollHeight)
    }


    //用户在文本域回车时响应
    /*function sendWSMessage() {
        if (event.keyCode === 13) {
            sendWSMsg();
        }
    }*/

    function sendWSMsg() {
        //获取fromName、toName、content以及msgType
        var msgContent = $("#input-msg").val();
        if (!msgContent) {
            $("#input-msg").val("请输入需要发送的内容!");
            return;
        }

        var msg = {};
        msg.fromUser = userSelf;
        msg.toUser = "广播" === selectedCheckUser ? "" : selectedCheckUser;
        msg.msgType = "message";
        msg.content = msgContent;
        $("#input-msg").val(msg.fromName + "=" + msg.toName + "" + msg.msgType + "" + msg.content)

        var msgJson = JSON.stringify(msg);
        ws.send(msgJson);
        $("#input-msg").val('');
    }


</script>