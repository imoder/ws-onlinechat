<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" pageEncoding="UTF-8" %>
<head>
    <meta charset="UTF-8">
    <title>Title</title>

    <link rel="shortcut icon" href="<c:url value="/static/iocn/index.ico"/>" type="image/x-icon"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://cdn.staticfile.org/jquery/1.10.2/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
            integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
            crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/js/bootstrap.min.js"
            integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
            crossorigin="anonymous"></script>
</head>
<body>

<!-- Button trigger modal -->
<button id="login-btn" type="button" class="btn btn-primary" data-toggle="modal" data-target="#staticBackdrop">
    Login!
</button>

<!-- Modal -->
<div class="modal fade" id="staticBackdrop" data-backdrop="static" data-keyboard="false" tabindex="-1"
     aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="staticBackdropLabel">Login frist please!</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label id="label1" for="username">name</label>
                        <input id="username" type="text" name="username" class="form-control"
                               aria-describedby="emailHelp">
                    </div>
                    <div class="form-group">
                        <label for="password">Password</label>
                        <input type="password" name="password" class="form-control" id="password">
                    </div>
                    <button type="button" class="btn btn-primary" onclick="loginSubmit()">登录</button>
                </form>
            </div>
        </div>
    </div>
</div>


</body>

<script>
    $(function () {
        $("#login-btn").click();
    })

    function loginSubmit() {
        $.ajax({
            url: document.location.pathname + "login",
            data: {username: $("#username").val(), password: $("#password").val()},
            type: "POST",
            dataType: "JSON",
            success: function (data) {
                if (data.success) {
                    //登录成功,
                    window.location.href = "onlinechat";
                    return;
                }
                alert("用户名或密码错误,登录失败!");
            },
            error: function (data) {
                alert("PAGE ERR." + data);
            }
        })
    }

</script>