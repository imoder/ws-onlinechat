<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" pageEncoding="UTF-8" %>
<head>
    <meta charset="UTF-8">
    <title>ERROR</title>
    <link rel="shortcut icon" href="<c:url value="/static/iocn/error.ico"/>" type="image/x-icon"/>
</head>
<body>
<h2>很遗憾,您访问的页面不存在!您可以跳转至
    <a href="<c:url value="/login"/>" style="text-decoration: none">登录页面</a>
</h2>

</body>