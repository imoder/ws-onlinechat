package com.zzb.config;

import com.zzb.interceptor.WebSocketHandShakeInterceptor;
import com.zzb.scoket.OnlineChatWebScoketHandler;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;

@Configuration
@EnableWebMvc
@EnableWebSocket
public class WebSocketConfig implements WebSocketConfigurer {
    @Override
    public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {
        // for thr original webscoket API.
        registry.addHandler(new OnlineChatWebScoketHandler(), "/websocket")
                .addInterceptors(new WebSocketHandShakeInterceptor()).setAllowedOrigins("*");
        // for the sockjs API.
        registry.addHandler(new OnlineChatWebScoketHandler(), "/sockjs/websocket")
                .addInterceptors(new WebSocketHandShakeInterceptor()).setAllowedOrigins("*").withSockJS();
    }
}
