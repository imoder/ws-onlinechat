package com.zzb.dao;

import com.zzb.pojo.User;

public interface UserDao {

    User findUserById(Integer id);

}
