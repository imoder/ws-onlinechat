package com.zzb.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.Map;

@Controller
public class LoginController {

    @RequestMapping(value = "/login")
    public @ResponseBody
    Map<String, Object> userLogin(String username, String password, HttpSession httpSession) {
        Map<String, Object> map = new HashMap<>();
        if (username != null && "123456".equals(password)) {
            map.put("success", true);
            httpSession.setAttribute("username", username);
            httpSession.setMaxInactiveInterval(3600 * 12);
        }
        System.out.println(username + ":" + password);
        return map;
    }

    @RequestMapping(value = "/onlinechat")
    public String onlinechat() {
        return "onlinechat";
    }


}
