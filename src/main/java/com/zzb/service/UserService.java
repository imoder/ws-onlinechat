package com.zzb.service;

import com.zzb.pojo.User;

public interface UserService {

    User findUserById(Integer id);

}
