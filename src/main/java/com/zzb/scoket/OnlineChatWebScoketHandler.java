package com.zzb.scoket;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.socket.*;

import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;

@SuppressWarnings("ALL")
public class OnlineChatWebScoketHandler implements WebSocketHandler {
    private static final Logger LOGGER = LoggerFactory.getLogger(OnlineChatWebScoketHandler.class);

    //session means WebSocketSession, httpSession means HTTPSession;
    private WebSocketSession session;
    private HttpSession httpSession;

    //These two flags are used to mark different types of messages (named:broadcast & message)
    private static final String TYPE_BOARDCAST = "boardcast";
    private static final String TYPE_User = "message";

    //for JSON parse&.ify
    private static final ObjectMapper JSONPASER = new ObjectMapper();

    /*
    * *Used to record all websocket connection instances.
    * In fact, every websokct connection will generate a connection instance and,
    * this class (OnlineChatWebScoketHandler) is just a 'SINGTON' in the SSM framework,
    * so the thought of just recording this class (OnlineChatWebScoketHandler) for getting all websockets by a field of 'userInstances' is error;
    * (err.)private static Map<HttpSession,OnlineChatWebScoketHandler> userInstances=new LinkedHashMap<>();
    * */
    private static Map<HttpSession, WebSocketSession> userInstances = new LinkedHashMap<>();

    // count the online user numbers;
    private int onlineUserCount;

    @Override
    public void afterConnectionEstablished(WebSocketSession webSocketSession) {
        /*
        * onopen hanlder
        * in fact, we may not need to create an additional global field to record it,
        * because we can get it in any method just by the variable 'webSocketSession';
        * */
        session = webSocketSession;
        httpSession = (HttpSession) webSocketSession.getAttributes().get(HttpSession.class.getName());
        String curUserName = (String) httpSession.getAttribute("username");
        LOGGER.debug("==>(＜(▰˘◡˘▰))==>cur request username:" + curUserName);

        //save current user information and session information!
        if (curUserName != null) {
            userInstances.putIfAbsent(httpSession, webSocketSession);
        }
        //get all online users' information, like this：'tom-smith-mary';
        String onlineUserNames = getAllOnlineUserNames();
        LOGGER.debug("==>(＜(▰˘◡˘▰))==>all online user names: " + onlineUserNames);

        /*
         * assembly information, returned as a Json string.
         * notes that, for simplification, the method is directly extracted in this class.
         * for better scalability, it should be extracted into a tool class!
         * The logic of assembling information is:
         *   1>BOARCAST：{type:"boardcast",fromName:"mary",toName:"",content:"tom-smith-mary"}
         *     or：{type:"boardcast",fromName:"",toName:"",content:"boardcast msg for all online users!"}
         *   2>MESSAGE：{type:"message",fromName:"xiaoming",toName:"xiaohu",content:"how are you!"}
         * */
        String msgContent = getMsgContent(TYPE_BOARDCAST, curUserName, "", onlineUserNames, "");
        LOGGER.debug("==>(＜(▰˘◡˘▰))==>msgContent: " + msgContent);

        //User online news broadcast, showing current user information to all online users!
        broadcastAllUsers(msgContent);

        //count++;
        incrOnlineUserCount();
    }

    @Override
    public void handleMessage(WebSocketSession webSocketSession, WebSocketMessage<?> webSocketMessage) throws Exception {
        //onmessage handler

        //{msgType:"message",fromUser:"Tom",toUser:"Smith",content:"h'a yee rectly?!"}
        //{msgType:"message",fromUser:"xiaoming",toUser:"",content:"this is a boardcast from xiaoming"}
        TextMessage msg = (TextMessage) webSocketMessage;
        LOGGER.debug("==>(＜(▰˘◡˘▰))==>msg from CLIENT: " + msg.getPayload());
        Map<String, String> msgMap = JSONPASER.readValue(msg.getPayload(), Map.class);
        String fromUser = msgMap.get("fromUser");
        String toUser = msgMap.get("toUser");
        String msgContent = msgMap.get("content");
        if ("".equals(toUser)) {
            //BOARDCAST
            broadcastAllUsers(((TextMessage) webSocketMessage).getPayload());
        } else {
            //MESSAGE
            pot2PotPushMesssage(fromUser, toUser, ((TextMessage) webSocketMessage).getPayload());
        }
    }

    @Override
    public void afterConnectionClosed(WebSocketSession webSocketSession, CloseStatus closeStatus) throws Exception {
        //onclose
        String curUserName = (String) httpSession.getAttribute("username");
        userInstances.keySet().removeIf(httpSession -> Objects.equals(webSocketSession, userInstances.get(httpSession)));
        String onlineUserNames = getAllOnlineUserNames();
        //user offline;
        String msgContent = getMsgContent(TYPE_BOARDCAST, curUserName, "", onlineUserNames, curUserName);
        LOGGER.warn("==>(＜(▰˘◡˘▰))==>from WS SERVER: " + curUserName + "has closed the WSConnection!");
        broadcastAllUsers(msgContent);
        decrOnlineUserCount();
    }

    @Override
    public void handleTransportError(WebSocketSession webSocketSession, Throwable throwable) throws Exception {
        //onerror handler
    }

    @Override
    public boolean supportsPartialMessages() {
        return false;
    }

    //broadcast to every onlineuser!
    private void broadcastAllUsers(String msgContent) {
        if (userInstances.size() > 0) {
            userInstances.forEach((httpSession, webSocketSession) -> {
                try {
                    if (webSocketSession.isOpen()) sendMessage2Client(webSocketSession, msgContent);
                    LOGGER.debug("==>(＜(▰˘◡˘▰))==>来自客户端: " + "broadcastAllUsers SUCCESS!");
                } catch (Exception e) {
                    e.printStackTrace();
                    LOGGER.error("==>(＜(▰˘◡˘▰))==>来自客户端: " + "broadcastAllUsers ERROR!");
                }
            });
        }
    }

    //pot2pot message!
    private void pot2PotPushMesssage(String fromUser, String toUser, String msgContent) {
        for (HttpSession httpsession : userInstances.keySet()) {
            //delivering the message to the message‘s sender and receiver
            if (toUser.equals(httpsession.getAttribute("username"))
                    || fromUser.equals(httpsession.getAttribute("username"))) {
                try {
                    LOGGER.error(userInstances.get(httpsession) + "=>" + toUser);
                    sendMessage2Client(userInstances.get(httpsession), msgContent);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void sendMessage2Client(WebSocketSession webSocketSession, String msg) {
        synchronized (webSocketSession) {
            try {
                webSocketSession.sendMessage(new TextMessage(msg));
                LOGGER.debug("==>(＜(▰˘◡˘▰))==>来自客户端: " + "sendMessage2Client SUCCESS!");
            } catch (IOException e) {
                e.printStackTrace();
                LOGGER.error("==>(＜(▰˘◡˘▰))==>来自客户端: " + "sendMessage2Client ERROR!");

            }
        }
    }

    //maybe better in a util package
    private String getMsgContent(String msgType, String fromUser, String toUser, String content, String outlineUser) {
        Map<String, String> tmp = new HashMap<>(4);
        tmp.put("msgType", msgType);
        tmp.put("fromName", fromUser);
        tmp.put("toName", toUser);
        tmp.put("content", content);
        tmp.put("flag", outlineUser);
        ObjectMapper om = new ObjectMapper();
        try {
            return JSONPASER.writeValueAsString(tmp);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return "{}";
    }

    private String getAllOnlineUserNames() {
        StringBuilder sb = new StringBuilder();
        if (userInstances.size() > 0) {
            for (HttpSession httpsession : userInstances.keySet()) {
                sb.append(httpsession.getAttribute("username")).append("-");
            }
        }
        return sb.toString().substring(0, sb.length() - 1);
    }

    public int getOnlineUserCount() {
        return onlineUserCount;
    }

    public void setOnlineUserCount(int onlineUserCount) {
        this.onlineUserCount = onlineUserCount;
    }

    public synchronized void incrOnlineUserCount() {
        this.onlineUserCount++;
    }

    public synchronized void decrOnlineUserCount() {
        this.onlineUserCount--;
    }

}
