package com.zzb.interceptor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.http.server.ServletServerHttpRequest;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.server.support.HttpSessionHandshakeInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Map;

public class WebSocketHandShakeInterceptor extends HttpSessionHandshakeInterceptor {
    private static final Logger LOGGER = LoggerFactory.getLogger(WebSocketHandShakeInterceptor.class);

    @Override
    public boolean beforeHandshake(ServerHttpRequest request, ServerHttpResponse response,
                                   WebSocketHandler wsHandler, Map<String, Object> attributes) throws Exception {
        //这里的参数request是webScoket连接的而不是Http的
        HttpServletRequest req = ((ServletServerHttpRequest) request).getServletRequest();

        /*An interceptor to copy information from the HTTP session to the "handshake
         * attributes" map to made available via{@link WebSocketSession#getAttributes()}
         *
         * 官方文档的这句话的意思是，通过拦截器中的*attributes*可以实现httpsession与webscoketsession的共享，
         * 那么我们如果为了在接下来的webscoket中使用用户保存在httpsession中的信息，那么在进行webscoket握手时可以
         * 将httpsession中的信息copy一份放在attributes中,再通过WebSocketSession#getAttributes()进行获取！
         *
         * 同样的，既然能拿到httpsession，其他的东西自然也可以拿到！O(∩_∩)O~
         * */
        attributes.put(HttpSession.class.getName(), req.getSession());
        LOGGER.debug(req.getRemoteAddr());
        LOGGER.debug("==>(＜(▰˘◡˘▰))==>set httpsession success: " +
                HttpSession.class.getName() + req.getSession().getAttribute("username") + ":" + req.getSession());
        //这里扩展的目的是为了兼容更多的浏览器
        if (request.getHeaders().containsKey("Sec-WebScoket-Extensions"))
            request.getHeaders().set("Sec-WebScoket-Extensions", "permessage-deflate");
        return super.beforeHandshake(request, response, wsHandler, attributes);
    }
}


