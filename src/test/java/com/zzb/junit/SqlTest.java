package com.zzb.junit;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.zzb.dao.UserDao;
import org.apache.ibatis.session.SqlSession;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.socket.TextMessage;

import java.util.HashMap;
import java.util.Map;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:applicationConfig.xml")
public class SqlTest {

    private static final ObjectMapper JSONPASER = new ObjectMapper();

    @Autowired
    SqlSession simpleSession;

    @Autowired
    SqlSession batchSession;

    @Test
    public void test() throws JsonProcessingException {
        Map<String,String> map=new HashMap<>();
        map.put("a","aaaa");map.put("b","bbbb");
        TextMessage textMessage = new TextMessage(JSONPASER.writeValueAsString(map));
        System.out.println(textMessage);
        System.out.println(textMessage.getPayload());


    }

}
